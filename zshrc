# Alias for ls.
alias la='ls -la'
alias li='ls -li'
alias lh='ls -lhA'

# Other Alias.
alias gp='grep -i'
alias his='history -c'
alias cat='bat --wrap auto --color always --decorations always'
alias v='vim'
alias vi='doas vim'
alias ac='acpi -b'
alias ec='echo'
alias ydp='yt-dlp -f'
alias clone='git clone'
alias viss='cli-visualizer'
alias free='free -m'
alias wtt='curl -4 wttr.in'


doas tee /sys/class/backlight/amdgpu_bl0/brightness <<< 60

# letters

figlet ' Hai!! '

toilet -t --gay -f ascii9 'Sentiasa, Oh-my-zsh ! '

toilet -t -f bigascii9 'jiwa kembar..'

toilet -t --metal -f bigmono9 ' ¡kenangan yang mendalam ! '


# param alias
search_man() {
         man $1 | grep -- $2
}

------- in the last --------

typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet
typeset -g POWERLEVEL9K_INSTANT_PROMPT=off

# Show prompt segment "kubecontext" only when the command you are typing
# invokes kubectl, helm, kubens, kubectx, oc, istioctl, kogito, k9s, helmfile, flux, fluxctl, stern, kubeseal, or skaffold.
typeset -g POWERLEVEL9K_KUBECONTEXT_SHOW_ON_COMMAND='kubectl|helm|kubens|kubectx|oc|istioctl|kogito|k9s|helmfile|flux|fluxctl|stern|kubeseal|skaffold'

